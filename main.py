import locale
import requests
import ast
import sys
import logging
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

locale.setlocale( locale.LC_ALL, '' )
sana = 'https://api.tgju.online/v1/data/sana/json'
api = requests.get(sana)
data = ast.literal_eval(api.content.decode('utf-8'))
key = open('./.env', 'r', encoding="utf-8")
token = key.read()
key.close()

curency_dic = {
    "usd_buy" : data["sana_buy_usd"],
    "usd_sell" : data["sana_sell_usd"],
    "gbp_buy": data["sana_buy_gbp"],
    "gbp_sell": data["sana_sell_gbp"],
    "eur_buy": data["sana_buy_eur"],
    "eur_sell": data["sana_sell_eur"]
}

def rate_graber(input='eur_buy'):
    date = curency_dic[input]["t"]
    title = curency_dic[input]["title"]
    rate = locale.currency(curency_dic[input]["p"]/10, grouping=True, symbol=False)
    return("{}:  {} تومان\n\n{}".format(title, rate, date))
#TODO: Tokenizer 
updater = Updater(token=token, use_context=True)
dispatcher = updater.dispatcher

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)
logger = logging.getLogger(__name__)

def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text=rate_graber())
def eur(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text=rate_graber("eur_buy"))
def usd(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text=rate_graber("usd_buy"))
def gbp(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text=rate_graber("gbp_buy"))

start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)
start_eur = CommandHandler('eur', eur)
dispatcher.add_handler(start_eur)
start_usd = CommandHandler('usd', usd)
dispatcher.add_handler(start_usd)
start_gbp = CommandHandler('gbp', gbp)
dispatcher.add_handler(start_gbp)

def caps(update, context):
    text_caps = ' '.join(context.args).upper()
    context.bot.send_message(chat_id=update.effective_chat.id, text=text_caps)

updater.start_polling()
updater.idle()
