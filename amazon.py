#!/usr/bin/env python3
import json
import requests
from bs4 import BeautifulSoup
from twilio.rest import Client
from fake_useragent import UserAgent
import time, random
from stem import Signal
from stem.control import Controller

# Load Credential data  for Twilio - Line 68
with open('cred.json', 'r') as file:
    #Confidential information
    config = json.load(file)
    account_sid = config['account_sid']
    auth_token = config['auth_token']

# Obtaining a new identity - Refer to README.md.
with Controller.from_port(port = 9051) as c:
    c.authenticate()
    c.signal(Signal.NEWNYM)
headers = { 'User-Agent': UserAgent().random }
proxies = {
    'http': 'socks5://127.0.0.1:9050',
    'https': 'socks5://127.0.0.1:9050'
}
# List of desireed Itms.
# TODO: Fixhardcode
urls = [
        "https://www.amazon.de/dp/B07R4MYTF6/?coliid=I2WQQ1YGBYFYXG&colid=1XEYCRQZQ0TJB&psc=1",
        "https://www.amazon.de/dp/B019PCF3QY/?coliid=I3FUX9DTDIMYLM&colid=1XEYCRQZQ0TJB&psc=1",
        "https://www.amazon.de/dp/B01NCWHD36/?coliid=IL8QQT7BWW2UG&colid=1XEYCRQZQ0TJB&psc=1",
        "https://www.amazon.de/Razer-Atheris-Mercury-Akkulaufzeit-optischer/dp/B07RQ8K8C6",
        "https://www.amazon.de/dp/B08425CZPG/?coliid=IG6H6QBG562PN&colid=1XEYCRQZQ0TJB&psc=1",
        "https://www.amazon.de/dp/B076QK6489/?coliid=I27B3BVJFRYVZT&colid=1XEYCRQZQ0TJB&psc=1",
        "https://www.amazon.de/Buffalo-DriveStation-lx2-0tu3-EU-External-Drive/dp/B01N0XT1RF",
      ]

# Select one random URL out of the list
def random_url():
    random_value = random.randint(0, len(urls)-1)
    return(urls[random_value])

# Scrape the URL and extract Price, title and discount if exist.
def price_graber(url):
    base_url = requests.get(url, headers=headers)
    soup = BeautifulSoup(base_url.content, 'html5lib')
    trigger = 0
    title = soup.find("span", id="productTitle").get_text().strip()
    if soup.find("span", class_="priceBlockStrikePriceString"):
        last_price = soup.find("span", class_="priceBlockStrikePriceString").get_text()
        trigger += 1
    price = soup.find("span", id="priceblock_ourprice").get_text()
    #price = price = math.floor(float(price[1:]))
    if soup.find("td", class_="priceBlockSavingsString"):
        save = soup.find("td", class_="priceBlockSavingsString").get_text().strip()
        trigger += 1
    if trigger == 2:
        return("{}\nLast Price:{}\nPrice: {}\nDiscount {}\n\n{}".format(
                                                                    title,
                                                                    last_price,
                                                                    price,
                                                                    save,
                                                                    url
                                                                        ))
    else:
        return("{}\nPrice: {}\n\n{}".format(title, price, url))

# Twilio sendst he message // Callback function
def push_notification():
    client = Client(account_sid, auth_token)
    message = client.messages.create(
                              from_='whatsapp:+14155238886',
                              body=price_graber(random_url()),
                              to='whatsapp:+491715169514'
                                    )


while True:
    push_notification()
    time.sleep((60*60) * 12)
#print(message.sid)